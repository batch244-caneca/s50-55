
/*import { useState, useEffect } from 'react';*/
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function Coursecard({courseProp}) {
	const { _id, name, description, price } = courseProp;

	//SYNTAX: const [getter, setter] = useState(initialGetterValue)
	/*const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [ isOpen, setIsOpen ] = useState(false)*/;
/*
	function enroll() {
		if (seats >=0) {
			setSeats(seats -1);
			setCount(count +1);
			console.log('Enrollees: ' + count);
			console.log('Seats ' + seats);
		} else {
			return alert('No more seats.')
		}
	}*/

	// Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
	// This is run automatically both after initial render and for every DOM update
	// React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update


	/*useEffect(() => {
		if (seats === 0) {
			setIsOpen(true);
		}
	}, [seats]);*/

	return (
		<Card>
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		         <Card.Text>PHP {price}</Card.Text>
		        {/* <Card.Text>Seats: {seats}</Card.Text>*/}
		        <Button as={Link} variant="primary" to={`/courses/${_id}`}>Details</Button>
		      </Card.Body>
		</Card>
	)
}