import { useContext } from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

  const {user} = useContext(UserContext);

  /*const [user, setUser] = useState(localStorage.getItem('email'));
  console.log(user);*/

  function refreshPage() {
    window.location.reload();
  }

  return (
    <Navbar bg="light" expand="lg">
        <Container>
            <Navbar.Brand as={Link} to='/'>Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

                {(user.id !== null)

                  ?
                  <Nav.Link as={NavLink} to="/logout" onClick={refreshPage}>Logout</Nav.Link>

                  :
                  <>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  </>
                }

              </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
  )
}

//export default AppNavbar (can also be used)