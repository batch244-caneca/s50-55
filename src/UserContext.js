import React from 'react';

const UserContext = React.createContext();
console.log(UserContext)

export const UserProvider = UserContext.Provider;

export default UserContext;
