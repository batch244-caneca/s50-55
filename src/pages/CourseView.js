import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import {useParams, Link} from 'react-router-dom';


export default function CourseView() {

	const { courseId } = useParams();

	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);

	useEffect(() => {
		console.log(courseId);

		fetch (`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [courseId]);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Successfully enrolled",
					icon: "success",
					text: "You have successfully enrolled to this course!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
						        <Card.Title>{name}</Card.Title>
						        <Card.Subtitle>Description</Card.Subtitle>
						        <Card.Text>{description}</Card.Text>
						        <Card.Subtitle>Price</Card.Subtitle>
						        <Card.Text>PHP {price}</Card.Text>
						        <Card.Subtitle>Class Schedule</Card.Subtitle>
						        <Card.Text>8 am - 5 pm</Card.Text>

						        {user.id !== null
						        	?
						        		<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
						        	:
						        		<Button as={Link} to="/login" variant="danger">Log in to Enroll</Button>
						        }
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}