/*import coursesData from '../data/coursesData';*/
import { useEffect, useState } from 'react';
import Coursecard from '../components/Coursecard';


export default function Courses() {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/courses`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				// Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
				setCourses(data.map(course => {
					return (
						<Coursecard key={course._id} courseProp = {course}/>
					)
				}))
			})
	}, [])


	/*const courses = coursesData.map(course => {
		return (
			<Coursecard key={course.id} courseProp = {course}/>
		)
	})*/

	return (
		<>
			<h1>Courses</h1>
			{courses}
			
		</>
	)
}