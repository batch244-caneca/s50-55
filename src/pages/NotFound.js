/*import {Link } from 'react-router-dom';
import {section} from 'react-bootstrap';

export default function NotFound(){

	return (
		<section className="section"> 
			<h1> Page not found</h1>
			<p> Go back to the <Link to='/'> homepage. </Link> </p>
		</section>
		)
}*/

import Banner from '../components/Banner';

export default function NotFound() {
	const data = {
		title: "404 - Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back home"
	}

	return (
		<Banner data={data}/>
	)
}